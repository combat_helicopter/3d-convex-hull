#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <queue>
#include <set>
#include <fstream>
#include <sstream>

double eps = 1e-8;

class rad_vec;
class facet;

class point {
public:
    point() = default;
    point(const point& p) : numb_(p.g_numb()), x_(p.g_x()), y_(p.g_y()), z_(p.g_z()){};
    point(const int& numb, const double& x, const double& y, const double& z) : numb_(numb), x_(x), y_(y), z_(z){};
public:
    int g_numb() const {return int(numb_);}
    double g_x() const {return double(x_);}
    double g_y() const {return double(y_);}
    double g_z() const {return double(z_);}
    std::string to_string() const;
public:
    bool operator==(const point& p)const;
    point& operator=(const point& p) = default;
private:
    int numb_ = 0;
    double x_ = 0;
    double y_ = 0;
    double z_ = 0;
};

class edge {
public:
    edge() = default;
    edge(const point& from, const point& to) : begin_(from), end_(to){};
public:
    rad_vec make_vec();
    point g_begin() const {return point(begin_);};
    point g_end() const {return point(end_);};
public:
    bool operator < (const edge& ed) const;
    edge& operator = (const edge& ed) = default;
private:
    point begin_;
    point end_;
};

class facet {
public:
    facet() = default;
    facet(const point& p1, const point& p2, const point& p3) : min_p_(p1), sec_p_(p2), third_p_(p3){};
public:
    edge return_edge(int numb_of_edge);
    edge return_invert_edge(int numb_of_edge);
    void order_points();
    point g_min_p() const {return point(min_p_);}
    point g_sec_p() const {return point(sec_p_);}
    point g_third_p() const {return point(third_p_);}
    void swap_inside(int field_one, int field_two);
public:
    bool operator < (const facet& fac) const;
private:
    point min_p_;
    point sec_p_;
    point third_p_;
};

class rad_vec {
public:
    rad_vec(double x, double y, double z) : x_(x), y_(y), z_(z) {};
    rad_vec(const point& p) : x_(p.g_x()), y_(p.g_y()), z_(p.g_z()) {};
public:
    double g_x() const {return double(x_);}
    double g_y() const {return y_;}
    double g_z() const {return z_;}
public:
    rad_vec operator+=(const rad_vec &v);
    rad_vec operator-=(const rad_vec &v);
    rad_vec operator+(const rad_vec &v);
    rad_vec operator-(const rad_vec &v);
    rad_vec operator=(const rad_vec &v);
    rad_vec operator*=(double scal);
    rad_vec operator*(double scal);
private:
    double x_;
    double y_;
    double z_;
};
//---------------------------------TOOLS----------------------------------------
rad_vec vec_prod(const rad_vec &v1, const rad_vec &v2) {
    double x = v1.g_y() * v2.g_z() - v1.g_z() * v2.g_y();
    double y = -(v1.g_x() * v2.g_z() - v1.g_z() * v2.g_x());
    double z = (v1.g_x() * v2.g_y() - v1.g_y() * v2.g_x());
    rad_vec prod(x, y, z);
    return prod;
}
//------------------------------------------------------------------------------
double scal_prod(const rad_vec &v1, const rad_vec &v2) {
    return v1.g_x() * v2.g_x() + v1.g_y() * v2.g_y() + v1.g_z() * v2.g_z();
}
//------------------------------------------------------------------------------
double length(const rad_vec& v) {
    return sqrt(scal_prod(v, v));
}
//------------------------------------------------------------------------------
double sin(const rad_vec &v1, const rad_vec &v2) {
    return length(vec_prod(v1, v2)) / length(v1) /length(v2);
}
//------------------------------------------------------------------------------
bool coord_cmp (const point& p1, const point& p2);

edge find_first_edge(std::vector<point>& points, int n) {
    std::sort(points.begin(), points.end(), coord_cmp);
    point next_point;
    point p0 = points[0];                                 //нашёл самую маленькую по OX, OY, OZ точку
    point p0_2D(p0.g_numb() , p0.g_x(), p0.g_y(), 0);  //спроецировал на OXY
    rad_vec oy(0, 1, 0);                        //направляющий вектор
    rad_vec v0(p0_2D);                                    //представил ввиде радиус вектора
    double min_sin = 2;
    for(int i = 1; i < n; ++i) {
        point p_i = points[i];
        rad_vec next(p_i.g_x(), p_i.g_y(), 0);  //перебираю точки
        rad_vec sub(next - v0);                             //вектор из v0 в next
        double sin_ = sin(sub, oy);
        if(min_sin - sin_ > eps) {                          //ищем наименьший или наибольший угол, нам подойдет любой
            min_sin = sin_;                                 //обновляем минимум
            next_point = points[i];                         //запоминаем точку
        }
    }
    return edge(p0, next_point);
}

facet find_first_facet(std::vector<point>& points, int n) {
    edge first_edge;
    point third_p;
    first_edge = find_first_edge(points, n);
    for(int i = 0; i < n; ++i) {                                    //найдем случайную точку
        if(points[i] == first_edge.g_begin() || points[i] == first_edge.g_end()) {
            continue;
        }
        third_p = points[i];
        break;
    }
    rad_vec v1(rad_vec(third_p) - rad_vec(first_edge.g_begin()));       //вектор из начала первого ребра в случайую точку
    for(int i = 0; i < n; ++i) {                                     //пройдемся по всем точкам
        if (points[i] == first_edge.g_begin() || points[i] == first_edge.g_end()) {
            continue;
        }
        rad_vec next_v1(rad_vec(points[i]) - rad_vec(first_edge.g_begin()));       //вектор из начала первого ребра претендента на точку в оболочке
        rad_vec prod = vec_prod(next_v1, v1);
        if(scal_prod(prod, first_edge.make_vec()) < eps) {                   //проверяем ориентацию
            v1 = next_v1;
            third_p = points[i];
        }
    }
    return facet(first_edge.g_begin(), first_edge.g_end(), third_p);
}

facet make_normalized_facet(std::vector<point>& points, int n) {
    facet first_facet = find_first_facet(points, n);
    point random_p;                                                                     //найдем случайную точку
    for(int i = 0; i < n; ++i) {
        if(points[i] == first_facet.g_min_p() || points[i] == first_facet.g_sec_p() || points[i] == first_facet.g_third_p()) {
            continue;
        }
        random_p = points[i];
        break;
    }
    first_facet.order_points();                                                         //поменяем порядок точек так, чтобы min_p была с наименьшим номером
    rad_vec prod = vec_prod(rad_vec(first_facet.g_sec_p()) - rad_vec(first_facet.g_min_p()),
                            rad_vec(first_facet.g_third_p()) - rad_vec(first_facet.g_min_p()));                 //проверим ориентацию
    double scal = scal_prod(prod, rad_vec(random_p) - rad_vec(first_facet.g_min_p()));
    if(scal > eps) {
        first_facet.swap_inside(2, 3);
    }
    return first_facet;
}



point& find_new_point(edge& ed, const std::set<edge>& edges_set, const facet& new_facet, point& new_point, int i){
    if(i == 1)                                                  //находим претендента на новую точку грани
        new_point = new_facet.g_third_p();
    if(i == 2)
        new_point = new_facet.g_min_p();
    if(i == 3)
        new_point = new_facet.g_sec_p();
    return new_point;
}


void new_facets(const std::vector<point>& points, int n, edge& ed, point& new_point, rad_vec& v1){
    for(int i = 0; i < n; ++i) {                                     //пройдемся по всем точкам
        if (points[i] == ed.g_begin() || points[i] == ed.g_end() || points[i] == new_point) {
            continue;
        }
        rad_vec next_v1(rad_vec(points[i]) - rad_vec(ed.g_begin()));       //вектор из начала первого ребра претендента на точку в оболочке
        rad_vec prod = vec_prod(next_v1, v1);
        if(scal_prod(prod, ed.make_vec()) < eps) {                   //проверяем ориентацию
            v1 = next_v1;
            new_point = points[i];
        }
    }
}

void build_convex_hull(std::vector<point>& points, std::vector<facet>& facets, int n) {
    facet first_facet = make_normalized_facet(points, n);
    std::queue<facet> facet_queue;
    std::set<edge>edges_set;
    facet_queue.push(first_facet);
    edges_set.insert(first_facet.return_edge(1));
    edges_set.insert(first_facet.return_edge(2));
    edges_set.insert(first_facet.return_edge(3));
    facets.push_back(first_facet);
    while(!facet_queue.empty()) {
        facet new_facet = facet_queue.front();
        facet_queue.pop();
        for(int i = 1; i < 4; ++i) {
            edge ed = new_facet.return_invert_edge(i);                      //вытащил перевернутое ребро из грани
            if(edges_set.find(ed) == edges_set.end()) {                     //проверил построена ли уже на нем грать
                point new_point = find_new_point(ed, edges_set, new_facet, new_point, i);
//--------------------------------------------------------------------------------------
                rad_vec v1(rad_vec(new_point) - rad_vec(ed.g_begin()));       //вектор из начала ed в случайую точку
                new_facets(points, n, ed, new_point,v1);
//---------------------------------------------------------------------------------------
                facet new_facet(ed.g_begin(), ed.g_end(), new_point);
                new_facet.order_points();
                facets.push_back(new_facet);
                facet_queue.push(new_facet);
                edges_set.insert(new_facet.return_edge(1));
                edges_set.insert(new_facet.return_edge(2));
                edges_set.insert(new_facet.return_edge(3));
            }
        }
    }
}

int main(int argc, char* argv[]) {
    int m = 0;
    int n = 0;
    std::ofstream out; // Поток out будем использовать для записи
    std::string output(argv[1]);
    std::cin >> m;
    out.open(output);
    out << m << std::endl;
    out.close();
    for (int i  = 0; i < m; ++i) {
        std::cin >> n;
        std::vector<point>test(n);
        std::vector<point>points(n);
        for(int j = 0; j < n; ++j) {
            double x;
            double y;
            double z;
            std::cin >> x >> y >> z;
            point p(j, x, y, z);
            test[j] = p;
            points[j] = p;
        }
        std::vector<facet> facets;
        build_convex_hull( points, facets, n);
        std::sort(facets.begin(), facets.end());
        size_t sz = facets.size();
        out.open(output, std::ios_base::app);
        out << sz << std::endl;
        for(int j = 0; j < facets.size(); ++j) {
            out << facets[j].g_min_p().to_string() << " ";
            out << facets[j].g_sec_p().to_string() << " ";
            out << facets[j].g_third_p().to_string() << std::endl;
        }
        out.close();
    }
}


bool coord_cmp (const point& p1, const point& p2){
    if(p1.g_x() - p2.g_x() < -eps)
        return true;
    if(fabs(p1.g_x() - p2.g_x()) < eps && p1.g_y() - p2.g_y() < -eps)
        return true;
    return fabs(p1.g_x() - p2.g_x()) < eps && fabs(p1.g_y() - p2.g_y()) < eps && p1.g_z() - p2.g_z() < -eps;
}

rad_vec rad_vec::operator=(const rad_vec &v) {
    this->x_ = v.g_x();
    this->y_ = v.g_y();
    this->z_ = v.g_z();
    return *this;
}

rad_vec rad_vec::operator+=(const rad_vec &v) {
    this->x_ += v.g_x();
    this->y_ += v.g_y();
    this->z_ += v.g_z();
    return *this;
}

rad_vec rad_vec::operator-=(const rad_vec &v) {
    this->x_ -= v.g_x();
    this->y_ -= v.g_y();
    this->z_ -= v.g_z();
    return *this;
}

rad_vec rad_vec::operator+(const rad_vec &v) {
    rad_vec ans = *this;
    ans += v;
    return ans;
}

rad_vec rad_vec::operator-(const rad_vec &v) {
    rad_vec ans = *this;
    ans -= v;
    return ans;
}

rad_vec rad_vec::operator*=(double scal) {
    this->x_ *= scal;
    this->y_ *= scal;
    this->z_ *= scal;
    return *this;
}

rad_vec rad_vec::operator*(double scal) {
    rad_vec ans = *this;
    ans *= scal;
    return ans;
}

std::string point::to_string() const {
    std::ostringstream str_stream;
    str_stream << numb_ << " " << x_ << " " << y_ << " " << z_;
    return str_stream.str();
}

bool point::operator==(const point& p) const{
    return this->numb_ == p.g_numb();
}

rad_vec edge::make_vec() {
    return rad_vec(this->end_) - rad_vec(this->begin_);
}

bool edge::operator < (const edge& ed) const{
    if(this->begin_.g_numb() < ed.begin_.g_numb())
        return true;
    return this->begin_.g_numb() == ed.begin_.g_numb() && this->end_.g_numb() < ed.end_.g_numb();
}

void facet::order_points() {
    int min_ = std::min(std::min(this->min_p_.g_numb(), this->sec_p_.g_numb()), this->third_p_.g_numb());
    if(fabs(this->min_p_.g_numb() - min_) < eps)
        return;
    if(fabs(this->sec_p_.g_numb() - min_) < eps) {
        std::swap(this->min_p_, this->sec_p_);
        std::swap(this->third_p_, this->sec_p_);
        return;
    }
    std::swap(this->min_p_, this->third_p_);
    std::swap(this->sec_p_, this->third_p_);
}

bool facet::operator < (const facet& fac) const {
    if(this->min_p_.g_numb() < fac.min_p_.g_numb())
        return true;
    if(this->min_p_.g_numb() == fac.min_p_.g_numb() && this->sec_p_.g_numb() < fac.sec_p_.g_numb())
        return true;
    return this->min_p_.g_numb() == fac.min_p_.g_numb() && this->sec_p_.g_numb() == fac.sec_p_.g_numb() &&
           this->third_p_.g_numb() < fac.third_p_.g_numb();
}

edge facet::return_edge(int numb_of_edge) {
    if(numb_of_edge == 1) {
        return edge(this->min_p_, this->sec_p_);
    }
    if(numb_of_edge == 2) {
        return edge(this->sec_p_, this->third_p_);
    }
    if(numb_of_edge == 3) {
        return edge(this->third_p_, this->min_p_);
    }
    return edge();
}

edge facet::return_invert_edge(int numb_of_edge) {
    if(numb_of_edge == 1) {
        return edge(this->sec_p_, this->min_p_);
    }
    if(numb_of_edge == 2) {
        return edge(this->third_p_, this->sec_p_);
    }
    if(numb_of_edge == 3) {
        return edge(this->min_p_, this->third_p_);
    }
    return edge();
}

void facet::swap_inside(int field_one, int field_two) {
    if(field_one * field_two == 2) {
        std::swap(min_p_, sec_p_);
    }
    if(field_one * field_two == 3) {
        std::swap(min_p_, third_p_);
    }
    if(field_one * field_two == 6) {
        std::swap(sec_p_, third_p_);
    }
}

