import numpy as np
import matplotlib.pyplot as plt
import sys
from mpl_toolkits import mplot3d

from collections import deque, Counter, defaultdict, namedtuple
# %matplotlib inline


class FacesReader:
    def __init__(self, file : str) -> None:
        self.hulls = []
        vec_len = 4
        with open(file) as f:
            self.count = int(f.readline())
            print()
            print("valuaes : ")
            print(self.count)
            print()
            for hulls in range(self.count):
                hull_size = int(f.readline())
                eges = []
                for _ in range(hull_size):
                    line = f.readline()
                    n1, x1, y1, z1, n2, x2, y2, z2, n3, x3, y3, z3 = map(int, line.split())
                    first_point = [n1, x1, y1, z1]
                    second_point = [n2, x2, y2, z2]
                    third_point = [n3, x3, y3, z3]
                    eges.append(np.array(sorted([first_point, second_point])))
                    eges.append(np.array(sorted([first_point, third_point])))
                    eges.append(np.array(sorted([second_point, third_point])))
                eges = np.unique(eges, axis=0)
                self.hulls.append(eges)

    def get_hull(self, numb : int):
        return np.unique(self.hulls[numb].reshape(self.hulls[numb].shape[0] * self.hulls[numb].shape[1], self.hulls[numb].shape[2]), axis=0).T
        #print(np.unique(self.eges.reshape(self.eges.shape[0] * self.eges.shape[1], self.eges.shape[2]), axis = 0).T)
        #print(self.eges.reshape(self.eges.shape[0] * self.eges.shape[1], self.eges.shape[2])[:, 1:].T)


class HullPainter:
    def __init__(self, numbs : np.array, xs : np.array, ys : np.array, zs : np.array, eges : np.array) -> None:
        self.numbs = numbs
        self.xs = xs
        self.ys = ys
        self.zs = zs
        self.eges = eges
        self.fig = plt.figure(num=0)
        self.ax = mplot3d.Axes3D(self.fig)

    def DrawFaces(self, ax):
        for ege in self.eges:
            print(ege.T[1:])
            ax.plot(*ege.T[1:])
        #ax.plot(*self.eges.reshape(self.eges.shape[0] * self.eges.shape[1], self.eges.shape[2])[:, 1:].T)

    @property
    def MakeAxes(self) -> mplot3d.Axes3D:
        return mplot3d.Axes3D(self.fig)

    def DrawPoints(self, ax):
        ax.scatter(self.xs, self.ys, self.zs)

    def DrawPointsAndFaces(self, ax):
        ax.plot(*self.eges.reshape(self.eges.shape[0] * self.eges.shape[1], self.eges.shape[2])[:, 1:].T)
        ax.scatter(self.xs, self.ys, self.zs)

file_name = sys.argv[1]
FR = FacesReader(file_name)

for i in range(FR.count):
    HR = HullPainter(*FR.get_hull(i), FR.hulls[i])
    HR.DrawFaces(HR.ax)

plt.show()
