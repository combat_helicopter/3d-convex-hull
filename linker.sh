#!bin/bash


if [[ ! -e $gcc_conv_hull_builder_output.txt ]]
then
	touch $gcc_conv_hull_builder_output.txt
fi

if [[ ! -e $gcc_conv_hull_builder ]]
then
	g++ build_hull.cpp -o gcc_conv_hull_builder
fi


./gcc_conv_hull_builder gcc_conv_hull_builder_output

python3 Painter.py gcc_conv_hull_builder_output
